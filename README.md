#ckpool front

##Installation
You need to install all dependencies. Node.js is required to do that.

First clone project
```shell
git clone https://bitbucket.org/fromcouch/ckpool-front.git
```

Then install:
```shell
npm install
```

After some minutes you have system ready.

##Project Structure
Project have next structure:

```
|-- dist
`-- src 
    |-- assets
    |-- components
    |-- controllers
    |-- helper
    `-- styles
```

* `/dist`: Contains transpiled files for distribution. Files here goes in the root folder of web server.
* `/src`: Source code
* `/src/assets`: have html files and assets
* `/src/components`: here are components that renders every page
* `/src/controllers`: controller that controls renders
* `/src/helpers`: helpers files
* `/src/styles`: sass styles


##Commands
Project is prepared with some defined commands to do basic operations (Generate dist files, development, etc ...)

###Generate Dist Files
Run next command:

```shell
npm run dist
```

This transpile project and make generated files in _dist_ folder

###Development server 
Run next command:

```shell
npm run serve
```

This executes internal webserver in port 3000. Then you can access server with:
```
http://localhost:3000
```

You can change port in **devServer** section of _webpack.config.dev.js_

```javascript
devServer: {
        port: 3000,
        contentBase: conf.dist
  }
```

###Lint
You can lint code. Actually project uses Airbnb rules that can be found here:

[https://github.com/airbnb/javascript](https://github.com/airbnb/javascript)

Command for lint:
```shell
npm run lint
```

Also you can run with auto fix option:
```shell
npm run lint-fix
```
