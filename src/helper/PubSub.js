/**
 * Created by victor on 21/01/2018.
 */

export default class PubSub {
    constructor() {
        this.handlers = [];
    }

    subscribe(event, handler, context, fixed = true) {
        if (typeof context === 'undefined') {
            handler.bind(handler);
        } else {
            handler.bind(context);
        }
        this.handlers.push({ event, handler, fixed });
    }

    unsubscribe(event) {
        this.handlers = this.handlers.filter(h => h.event !== event);
    }

    publish(event, args) {
        this.handlers.forEach((topic) => {
            if (topic.event === event) {
                topic.handler(args);
            }
        });
    }

    resetFixed() {
        this.handlers = this.handlers.filter(x => x.fixed === true);
    }
}
