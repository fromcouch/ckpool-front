
/* eslint no-console: "off" */
export default class utils {
    static guid() {
        return this.s4() + this.s4() + this.s4() + this.s4() +
            this.s4() + this.s4() + this.s4() + this.s4();
    }

    static s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    static no(value) {
        return `<div class="u-center">${value}</div>`;
    }

    static log(...value) {
        console.log(value);
    }

    static convertToRedeable(number) {
        // return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

        const parts = number.toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return parts.join('.');
    }
}
