/**
 * Created by victor on 04/12/2017.
 */
import Form from 'form-to-json';
// import utils from './utils';

export default class dom {
    static select(selector) {
        return document.querySelector(selector);
    }

    static selectAll(selector) {
        return [...document.querySelectorAll(selector)];
    }

    static appendText(selector, text) {
        document.querySelector(selector).insertAdjacentHTML('beforeend', text);
    }

    static text(selector, text) {
        const elementList = [...document.querySelectorAll(selector)];
        elementList.forEach((e) => {
            e.innerText = text;
        });
    }

    static appendHtml(selector, html) {
        const elementList = [...document.querySelectorAll(selector)];
        elementList.forEach((e) => {
            e.innerHTML += html;
        });
    }

    static html(selector, html) {
        const elementList = [...document.querySelectorAll(selector)];
        elementList.forEach((e) => {
            e.innerHTML = html;
        });
    }

    static addClass(selector, clase) {
        const el = this.select(selector);
        el.className = el.className.trim();
        el.className += ` ${clase}`;
    }

    static removeClass(selector, clase) {
        const elems = this.selectAll(selector);
        elems.forEach((elem) => {
            const e = elem;
            e.className = elem.className.replace(clase, '');
        });
    }

    /**
     * Get first element by name in dom
     * @param name
     * @returns {*}
     */
    static getFirstByName(name) {
        const elems = this.getAllByName(name);
        if (elems && elems.length > 0) {
            return elems[0];
        }

        return false;
    }

    /**
     * Get all elements by name in dom
     * @param name
     * @returns {NodeList}
     */
    static getAllByName(name) {
        return document.getElementsByName(name);
    }

    /**
     * Get first element by className in dom
     * @param className
     * @returns {*}
     */
    static getFirstByClass(className) {
        const elems = this.getAllByClass(className);
        if (elems && elems.length > 0) {
            return elems[0];
        }

        return false;
    }

    /**
     * Get all elements by className in dom
     * @param className
     * @returns {NodeList}
     */
    static getAllByClass(className) {
        return document.getElementsByClassName(className);
    }

    /**
     * Get element by id
     * @param id
     * @returns {Element}
     */
    static getById(id) {
        return document.getElementById(id);
    }

    static post(url, data, pid) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Authentication', pid);
        xhr.onload = () => {};
        xhr.send(JSON.stringify(data));
    }

    static get(url) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.onload = (data) => {
                resolve(data.currentTarget.response);
            };

            xhr.onerror = (error) => {
                reject(error);
            };

            xhr.send();
        });
    }

    static getSync(url) {
        return new Promise((resolve, reject) => {
            try {
                const xhr = new XMLHttpRequest();
                xhr.open('GET', url, false);
                xhr.send();

                resolve(xhr.responseText);
            } catch (err) {
                reject(err);
            }
        });
    }

    static resolve(value) {
        return value;
    }

    static reject(error) {
        return error;
    }

    static async g(url, async = true) {
        let res;

        if (async) {
            res = await this.get(url).then(this.resolve, this.reject);
        } else {
            res = await this.getSync(url).then(this.resolve, this.reject);
        }

        return res;
    }

    static formToJson(form) {
        return Form(form).toJson();
    }
}
