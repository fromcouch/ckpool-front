/**
 * Created by victor on 10/01/2018.
 */

import dom from '../helper/dom';
import footer from '../assets/footer.html';

export default class HeaderRender {
    constructor() {
        this.html = footer;
    }

    draw() {
        dom.html('footer', this.html);
    }
}
