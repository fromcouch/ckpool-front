/**
 * Created by victor on 10/01/2018.
 */

import moment from 'moment';
import store from 'store';
import expire from 'store/plugins/expire';

import dom from '../helper/dom';
import util from '../helper/utils';

import poolstatus from '../assets/poolstatus.html';

export default class DashboardRender {
    constructor(ps) {
        this.html = poolstatus;
        ps.subscribe('showData', this.constructor.fillData, this, false);
        store.addPlugin(expire);
    }

    draw(config) {
        dom.html('.main', this.html);
        this.drawPayouts(config);
    }

    drawPayouts(config) {
        const me = this;
        const p = store.get('ckpool-work');

        if (p !== undefined) {
            me.constructor.fillWork(p);
        } else {
            const data = dom.g(`${config.server}${config.work}`).catch(error => error);
            data.then((d) => {
                const payouts = JSON.parse(d);
                store.set('ckpool-work', payouts, new Date().getTime() + 900000);
                me.constructor.fillWork(payouts);
            });
        }
    }

    static fillWork(payouts) {
        dom.html('.main-work-height', util.convertToRedeable(payouts.height));
        dom.html('.main-work-fee', payouts.fee);
        dom.html('.main-work-reward', payouts.reward);
        dom.html('.main-work-herp', payouts.herp);

        Object.keys(payouts.payouts).forEach((p) => {
            const line = `
                    <tr>
                        <td></td>
                        <td>${p}</td>
                        <td>${payouts.payouts[p]}</td>
                    </tr>
                `;
            dom.appendHtml('.main-work-reward-list > tbody', line);
        });
    }

    static fillData(data) {
        const runtime = moment.duration(data[0].runtime * 1000);
        const lastupdate = moment(data[0].lastupdate * 1000).fromNow();

        dom.text('.main-data-runtime', runtime.humanize());
        dom.text('.main-data-lastupdate', lastupdate);
        dom.text('.main-data-users', data[0].Users);
        dom.text('.main-data-workers', data[0].Workers);
        dom.text('.main-data-idle', data[0].Idle);
        dom.text('.main-data-disconnected', data[0].Disconnected);
        dom.text('.main-data-accepted', `${util.convertToRedeable(data[3].accepted)} hashes`);
        dom.text('.main-data-rejected', `${util.convertToRedeable(data[3].rejected)} hashes`);
        dom.text('.main-data-diff', data[3].diff);
        dom.text('.main-data-lns', util.convertToRedeable(data[3].lns));
        dom.text('.main-data-herp', util.convertToRedeable(data[3].herp));
        dom.text('.main-data-reward', `${data[3].reward} BTC`);

        dom.text('.main-data-hashrate1m', `${data[1].hashrate1m}H/s`);
        dom.text('.main-data-hashrate5m', `${data[1].hashrate5m}H/s`);
        dom.text('.main-data-hashrate15m', `${data[1].hashrate15m}H/s`);
        dom.text('.main-data-hashrate1h', `${data[1].hashrate1hr}H/s`);
        dom.text('.main-data-hashrate6h', `${data[1].hashrate6hr}H/s`);
        dom.text('.main-data-hashrate1d', `${data[1].hashrate1d}H/s`);
        dom.text('.main-data-hashrate7d', `${data[1].hashrate7d}H/s`);
        dom.text('.main-data-sps1m', data[2].SPS1m);
        dom.text('.main-data-sps5m', data[2].SPS5m);
        dom.text('.main-data-sps15m', data[2].SPS15m);
        dom.text('.main-data-sps1h', data[2].SPS1h);
    }
}
