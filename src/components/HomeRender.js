/**
 * Created by victor on 21/01/2018.
 */

import home from '../assets/home.html';
import dom from '../helper/dom';

export default class HomeRender {
    constructor(ps) {
        this.ps = ps;
        this.html = home;
    }

    draw() {
        dom.html('.main', this.html);
    }
}
