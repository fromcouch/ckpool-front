/**
 * Created by victor on 08/02/2018.
 */

// import moment from 'moment';

import dom from '../helper/dom';
import util from '../helper/utils';

import miners from '../assets/miners.html';

export default class MinersRender {
    constructor() {
        this.html = miners;
    }

    draw(config) {
        dom.html('.main', this.html);
        this.drawMiners(config);
    }

    drawMiners(config) {
        const me = this;

        const data = dom.g(`${config.server}${config.miners}`).catch(error => error);
        data.then((d) => {
            const minersData = d.split(/\r?\n/);
            minersData.splice(-1, 1);

            const m = minersData.map(e => JSON.parse(`{"${e.replace(':', '":')}}`));
            me.constructor.fillWork(m);
        });
    }

    static fillWork(m) {
        m.forEach((p, i) => {
            const miner = Object.entries(p);
            const btcAddress = miner[0][0];
            const data = miner[0][1];
            const line = `
                    <tr>
                        <td>${i + 1}</td>
                        <td>${btcAddress}</td>
                        <td>${data.hashrate1hr}H/s</td>
                        <td>${data.hashrate1d}H/s</td>
                        <td>${data.hashrate7d}H/s</td>
                        <td>${data.workers}</td>
                        <td>${util.convertToRedeable(data.shares)}</td>
                        <td>${data.bestshare}</td>
                        <td>${util.convertToRedeable(data.luck * 10 * 10)}%</td>
                        <td>${util.convertToRedeable(data.herp.toFixed(0))}</td>
                        <td>${data.derp}</td>
                    </tr>
                `;
            dom.appendHtml('.main-miners-list > tbody', line);
        });

        dom.getFirstByClass('main-miners-list').deleteRow(1);
    }
}
