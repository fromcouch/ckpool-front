/**
 * Created by victor on 10/01/2018.
 */

import dom from '../helper/dom';
import header from '../assets/header.html';

export default class HeaderRender {
    constructor(ps) {
        this.html = header;
        ps.subscribe('showData', this.constructor.fillData, this, true);
    }

    draw() {
        dom.html('header', this.html);
        this.bindEvents();

        if (document.location.hash !== '') {
            dom.select(`.ebavs-menu a[href*="${document.location.hash}"]`).parentNode.className = 'active';
        } else {
            dom.select('.ebavs-menu a[href="/#/"]').parentNode.className = 'active';
        }
    }

    static fillData(data) {
        dom.text('.header-pool-hashrate', `${data[1].hashrate1d}H/s`);
        dom.text('.header-pool-sps', data[2].SPS1h);
    }

    bindEvents() {
        const me = this;

        dom.getFirstByClass('button-collapse').addEventListener('click', (event) => {
            event.preventDefault();
            // dom.select('#mobile-menu').style.transform = 'translateX(0)';
            // dom.select('#mobile-menu').style.transition = 'transform .5s ease';
            dom.addClass('#mobile-menu', 'show-menu');
            dom.removeClass('.overlay-nav', 'hide');
        });

        dom.getFirstByClass('overlay-nav').addEventListener('click', () => {
            me.constructor.hideResponsiveMenu();
        });

        dom.select('#mobile-menu').addEventListener('click', () => {
            me.constructor.hideResponsiveMenu();
        });

        const menuElements = dom.selectAll('.ebavs-menu li');
        menuElements.forEach(e => e.addEventListener('click', () => {
            dom.removeClass('.ebavs-menu li', 'active');
            e.className = 'active';
        }));
    }

    static hideResponsiveMenu() {
        dom.addClass('.overlay-nav', 'hide');
        dom.removeClass('#mobile-menu', 'show-menu');
    }
}
