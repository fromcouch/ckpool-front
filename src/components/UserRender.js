/**
 * Created by victor on 11/01/2018.
 */

import moment from 'moment';
import store from 'store';
import Chart from 'chart.js';

import dom from '../helper/dom';
import util from '../helper/utils';
import us from '../assets/user.html';

/* eslint no-param-reassign: "off" */

export default class UserRender {
    constructor(ps) {
        this.html = us;
        this.ps = ps;
        this.data = {};
        this.btc = null;
        this.actualData = {};
        this.chart = null;
        this.order = null;
        // ps.subscribe('showData', this.constructor.fillData, this, false);
    }

    draw(config) {
        dom.html('.main', this.html);
        this.bindEvents(config);
        const btc = store.get(`${config.server}-btcaddress`);

        if (btc) {
            this.btc = btc;
            dom.getFirstByClass('main-user-status-address').value = btc;
            this.searchUser(config, btc);
            this.initChart();
        }
    }

    fillData() {
        dom.html('.main-user-hashrate', `${this.actualData.hashrate1d}H/s`);
        dom.html('.main-user-hashrate-1m', `${this.actualData.hashrate1m}H/s`);
        dom.html('.main-user-hashrate-5m', `${this.actualData.hashrate5m}H/s`);
        dom.html('.main-user-hashrate-1h', `${this.actualData.hashrate1hr}H/s`);
        dom.html('.main-user-hashrate-1w', `${this.actualData.hashrate7d}H/s`);
        dom.html('.main-user-shares', util.convertToRedeable(this.actualData.shares));
        dom.html('.main-user-lns', util.convertToRedeable(this.actualData.lns));
        dom.html('.main-user-herp', util.convertToRedeable(this.actualData.herp));
        dom.html('.main-user-luck', `${util.convertToRedeable(this.actualData.luck * 100)} %`);
        dom.html('.main-user-lastshare', this.actualData.lastshare);
        dom.html('.main-user-bestshare', this.actualData.bestshare);
        dom.html('.main-user-derp', this.actualData.derp);

        dom.removeClass('.main-user', 'hide');
        this.initWorkers();
        this.updateChart();
    }

    fillWorkers(page) {
        const me = this;
        if (this.actualData.worker.length > 0) {
            dom.html('.main-user-workers > tbody', '');

            let workers = this.constructor.removeInactiveWorkers(this.actualData.worker);

            if (this.order) {
                workers = workers.sort(this.constructor.sortBy(this.order.element, this.order.target.getAttribute('data-order') === 'desc', this.order.callback));
            }

            const ddl = dom.getFirstByClass('results-number');
            const result = ddl.options[ddl.selectedIndex].value;
            const totalPages = Math.ceil(workers.length / result);

            const pagination = this.constructor.pagination(page, totalPages);

            const maxWorkers = page > 0 ? page * result : workers.length;
            const initValue = page > 0 ? (page - 1) * result : 0;

            for (let index = initValue; index < maxWorkers; index += 1) {
                const w = workers[index];

                const line = `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${w.workername}</td>
                        <td>${w.hashrate1m}</td>
                        <td>${w.hashrate1hr}</td>
                        <td>${w.hashrate1d}</td>
                        <td>${util.convertToRedeable(w.shares)}</td>
                        <td>${moment(w.lastshare * 1000).fromNow()}</td>
                    </tr>`;
                dom.appendHtml('.main-user-workers > tbody', line);
            }

            dom.getFirstByClass('pagination-numbers').innerHTML = '';
            pagination.forEach((p, index) => {
                let l = `
                    <a class="pag-number" data-page="${p}" href="#">${p}</a> 
                `;

                if (p === '...') {
                    l = ' ... ';
                }

                if (p === page) {
                    l = page;
                }

                if (index < pagination.length - 1) {
                    l += ',';
                }
                dom.appendHtml('.pagination-numbers', l);
            });

            const elems = [...dom.getAllByClass('pag-number')];
            elems.forEach((e) => {
                e.addEventListener('click', (event) => {
                    event.preventDefault();

                    me.fillWorkers(event.srcElement.getAttribute('data-page'));
                });
            });
        }

        dom.removeClass('.workers-list', 'hide');
    }

    initWorkers() {
        if (this.actualData.worker.length > 50) {
            dom.removeClass('.max-results', 'hide');
            dom.removeClass('.dead-workers', 'hide');
            dom.removeClass('.pagination', 'hide');

            const reload = () => {
                this.fillWorkers(1);
            };
            const results = dom.getFirstByClass('results-number');
            results.removeEventListener('change', reload);
            results.addEventListener('change', reload);

            const dead = dom.getFirstByClass('dead-workers-check');
            dead.removeEventListener('click', reload);
            dead.addEventListener('click', reload);
            this.fillWorkers(1);
        } else {
            this.fillWorkers(0);
        }
    }

    updateChart() {
        // this.chart;
        // this.constructor.unhuman(this.actualData.hashrate1m),

        let data = store.get(`ckpoolchart-${this.btc}`);
        if (!data) {
            data = {
                h5m: [],
                h1hr: [],
                h1d: [],
                count: 0,
            };
        }

        if (data.count === 0 || data.count === 10) {
            const date = new Date();
            const h5m = {
                // x: date,
                t: date,
                y: this.constructor.unhuman(this.actualData.hashrate5m),
            };

            const h1hr = {
                // x: date,
                t: date,
                y: this.constructor.unhuman(this.actualData.hashrate1hr),
            };

            const h1d = {
                // x: date,
                t: date,
                y: this.constructor.unhuman(this.actualData.hashrate1d),
            };

            if (this.chart && this.chart.data) {
                this.chart.data.datasets.forEach((dataset) => {
                    switch (dataset.label) {
                    case 'Hashrate 5 minute':
                        dataset.data.push(h5m);
                        break;

                    case 'Hashrate 1 hour':
                        dataset.data.push(h1hr);
                        break;

                    case 'Hashrate 1 day':
                        dataset.data.push(h1d);
                        break;

                    default:
                        break;
                    }
                });
                this.chart.update();
            }
            data.h5m.push(h5m);
            data.h1hr.push(h1hr);
            data.h1d.push(h1d);

            data.count = 0;
        }

        data.count += 1;
        store.set(`ckpoolchart-${this.btc}`, data);
    }

    initChart() {
        // https://www.chartjs.org/docs/latest/axes/cartesian/time.html
        // http://jsfiddle.net/prfd1m8q/
        // http://www.chartjs.org/samples/latest/
        store.remove('timecharter');
        const ctx = document.getElementById('myChart').getContext('2d');

        const labels = [];
        for (let t = 11; t >= 0; t -= 1) {
            labels.push(moment().subtract(t, 'hours').toDate().getTime());
        }
        const data = this.getChartData();
        this.chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels,
                datasets: [{
                    label: 'Hashrate 5 minute',
                    backgroundColor: 'rgb(255, 159, 64)',
                    data: data.h5m,
                    fill: false,
                },
                {
                    label: 'Hashrate 1 hour',
                    backgroundColor: 'rgb(54, 162, 235)',
                    data: data.h1hr,
                    fill: false,
                },
                {
                    label: 'Hashrate 1 day',
                    backgroundColor: 'rgb(75, 192, 192)',
                    data: data.h1d,
                    fill: false,
                },
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: true,
                },
                title: {
                    display: true,
                    text: 'Hashrate',
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'hour',
                            tooltipFormat: 'lll',
                            min: labels[0],
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            callback: label => this.constructor.human(label),
                        },
                    }],
                },
            },
        });
        data.count = 0;
        store.set(`ckpoolchart-${this.btc}`, data);
    }

    getChartData() {
        let data = store.get(`ckpoolchart-${this.btc}`);
        if (!data) {
            data = {
                h5m: [],
                h1hr: [],
                h1d: [],
            };
        }
        return data;
    }

    bindEvents(config) {
        const me = this;
        dom.getFirstByClass('main-user-status-button').addEventListener('click', (event) => {
            event.preventDefault();

            const address = dom.getFirstByClass('main-user-status-address').value;
            if (address) {
                this.searchUser(config, address);
            } else {
                dom.addClass('.main-user-status-address', 'invalid');
            }
        });

        dom.getFirstByClass('column-totalshares').addEventListener('click', (event) => {
            event.preventDefault();

            this.order = {
                target: event.target,
                element: 'shares',
                callback: parseInt,
            };
            this.orderBy();
            /* dom.select('.column-totalshares > i').innerHTML =
                event.target.getAttribute('data-order') === 'desc' ?
                    'arrow_drop_down' :
                    'arrow_drop_up'; */
        });

        dom.getFirstByClass('column-hash').addEventListener('click', (event) => {
            event.preventDefault();

            this.order = {
                target: event.target,
                element: 'hashrate1m',
                callback: me.constructor.unhuman,
            };
            this.orderBy();
            /* dom.select('.column-hash > i').innerHTML =
                event.target.getAttribute('data-order') === 'desc' ?
                    'arrow_drop_down' :
                    'arrow_drop_up'; */
        });

        dom.getFirstByClass('column-hash1hr').addEventListener('click', (event) => {
            event.preventDefault();

            this.order = {
                target: event.target,
                element: 'hashrate1hr',
                callback: me.constructor.unhuman,
            };
            this.orderBy();
            /* dom.select('.column-hash1hr > i').innerHTML =
                event.target.getAttribute('data-order') === 'desc' ?
                    'arrow_drop_down' :
                    'arrow_drop_up'; */
        });

        dom.getFirstByClass('column-hash1d').addEventListener('click', (event) => {
            event.preventDefault();

            this.order = {
                target: event.target,
                element: 'hashrate1d',
                callback: me.constructor.unhuman,
            };
            this.orderBy();
            /* dom.select('.column-hash1d > i').innerHTML =
                event.target.getAttribute('data-order') === 'desc' ?
                    'arrow_drop_down' :
                    'arrow_drop_up'; */
        });
    }

    orderBy() {
        if (this.order) {
            const pagination = this.actualData.worker.length > 50 ? 1 : 0;

            let order = this.order.target.getAttribute('data-order');

            if (order && order === 'desc') {
                order = 'asc';
                this.order.target.innerHTML = 'arrow_drop_up';
            } else {
                order = 'desc';
                this.order.target.innerHTML = 'arrow_drop_down';
            }

            this.order.target.setAttribute('data-order', order);
            this.actualData.worker = this.actualData.worker.sort(this.constructor.sortBy(this.order.element, order === 'desc', this.order.callback));

            this.fillWorkers(pagination);
        }
    }

    static unhuman(text) {
        const powers = {
            0: 0,
            k: 1,
            m: 2,
            g: 3,
            t: 4,
            p: 5,
            e: 6,
            z: 7,
            y: 8,
        };
        const regex = /(\d+(?:\.\d+)?)\s?(k|m|g|t|p|e|z|y)?b?/i;

        const res = regex.exec(text);
        if (!res[2]) {
            res[2] = '0';
        }

        return res[1] * (1024 ** powers[res[2].toLowerCase()]);
    }

    static human(number) {
        const i = number === 0 ? 0 : Math.floor(Math.log(number) / Math.log(1024));
        const total = (number / (1024 ** i)).toFixed(1) * 1;
        const size = ['H', 'kH', 'MH', 'GH', 'TH', 'PH', 'EH', 'ZH', 'YH'][i];
        return `${total}${size}`;
    }

    searchUser(config, address) {
        const me = this;

        store.set(`${config.server}-btcaddress`, address);
        const url = `${config.server}${config.user}${address}`;
        this.getUserData(url);
        if (this.timeoutId) {
            clearInterval(this.timeoutId);
        }
        this.timeoutId = setInterval(() => {
            me.getUserData(url);
        }, config.every);
    }

    getUserData(url) {
        const me = this;
        const ud = this.constructor.callData(url);
        ud.then((workers) => {
            me.actualData = workers;
            me.fillData();
        });
    }

    static async callData(url) {
        const data = await dom.g(url, false).catch(error => error);
        return JSON.parse(data);
    }

    static pagination(currentPage, pageCount) {
        const delta = 2;
        const left = currentPage - delta;
        const right = currentPage + delta + 1;

        const result = Array.from({ length: pageCount }, (v, k) => k + 1)
            .filter(i => i && i >= left && i < right);

        if (result.length > 1) {
            // Add first page and dots
            if (result[0] > 1) {
                if (result[0] > 2) {
                    result.unshift('...');
                }
                result.unshift(1);
            }

            // Add dots and last page
            if (result[result.length - 1] < pageCount) {
                if (result[result.length - 1] !== pageCount - 1) {
                    result.push('...');
                }
                result.push(pageCount);
            }
        }

        return result;
    }

    static removeInactiveWorkers(workers) {
        let ws;
        if (dom.getFirstByClass('dead-workers-check').checked && workers.length > 50) {
            ws = workers.filter((w) => {
                const m = moment().subtract(7, 'days').startOf('day');
                return !m.isAfter(moment(w.lastshare * 1000));
            });
        } else {
            ws = workers;
        }

        return ws;
    }

    static sortBy(field, reverse, primer) {
        const key = primer ?
            x => primer(x[field]) :
            x => x[field];

        reverse = !reverse ? 1 : -1;

        return (a, b) => {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }
}
