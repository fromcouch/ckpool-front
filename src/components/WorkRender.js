/**
 * Created by victor on 08/02/2018.
 */

import store from 'store';
import expire from 'store/plugins/expire';

import dom from '../helper/dom';
import util from '../helper/utils';

import work from '../assets/work.html';

export default class DashboardRender {
    constructor() {
        this.html = work;
        store.addPlugin(expire);
    }

    draw(config) {
        dom.html('.main', this.html);
        this.drawPayouts(config);
    }

    drawPayouts(config) {
        const me = this;
        const p = store.get('ckpool-work');

        if (p !== undefined) {
            me.constructor.fillWork(p);
        } else {
            const data = dom.g(`${config.server}${config.work}`).catch(error => error);
            data.then((d) => {
                const payouts = JSON.parse(d);
                store.set('ckpool-work', payouts, new Date().getTime() + 900000);
                me.constructor.fillWork(payouts);
            });
        }
    }

    static fillWork(payouts) {
        dom.html('.main-work-height', util.convertToRedeable(payouts.height));
        dom.html('.main-work-fee', payouts.fee);
        dom.html('.main-work-reward', payouts.reward);
        dom.html('.main-work-herp', util.convertToRedeable(payouts.herp));

        let lines = '';
        Object.keys(payouts.payouts).forEach((p, i) => {
            lines += `
                    <tr>
                        <td>${i + 1}</td>
                        <td>${p}</td>
                        <td>${payouts.payouts[p]}</td>
                    </tr>
                `;
        });
        dom.html('.main-work-reward-list > tbody', lines);

        lines = '';
        Object.keys(payouts.postponed).forEach((p, i) => {
            lines += `
                    <tr>
                        <td>${i + 1}</td>
                        <td>${p}</td>
                        <td>${util.convertToRedeable(payouts.postponed[p])}</td>
                    </tr>
                `;
        });
        dom.html('.main-work-postponed-list > tbody', lines);
    }
}
