/**
 * Created by victor on 30/11/2017.
 */

/* global document */
/* eslint-disable eol-last */
/* eslint no-undef: "off" */

import 'babel-polyfill';

import Router from 'es6-router';
import store from 'store';

import './styles/styles.scss';

import PubSub from './helper/PubSub';
import HeaderRender from './components/HeaderRender';
import FooterRender from './components/FooterRender';
import HomeRender from './components/HomeRender';
import DashboardRender from './components/DashboardRender';
import UserRender from './components/UserRender';
import WorkRender from './components/WorkRender';
import MinersRender from './components/MinersRender';
import RenderController from './controllers/RenderController';
import config from './config';

const btc = store.get(`${config.server}-btcaddress`);

window.addEventListener('load', () => {
    const ps = new PubSub();
    const rc = new RenderController(config, ps);
    const h = new HeaderRender(ps);
    const f = new FooterRender();
    let user = null;

    h.draw();
    f.draw();

    if (btc) {
        user = new UserRender(ps);
        user.searchUser(config, btc);
    }

    const router = new Router()
        .add('/', () => {
            const home = new HomeRender(ps);
            home.draw();
            rc.run();
        })
        .add(/dashboard/, () => {
            const dash = new DashboardRender(ps);
            dash.draw(config);
            rc.run();
        })
        .add(/work/, () => {
            const work = new WorkRender();
            work.draw(config);
            rc.run();
        })
        .add(/miners/, () => {
            const miners = new MinersRender();
            miners.draw(config);
            rc.run();
        })
        .add(/user/, () => {
            if (!user) {
                user = new UserRender(ps);
            }
            user.draw(config);
            rc.run();
        });

    router.listen();
    rc.init();
});
