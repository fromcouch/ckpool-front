/**
 * Created by victor on 15/12/2017.
 */

const config = {
    server: 'http://ckpool.org/',
    status: 'pool/pool.status',
    work: 'pool/pool.work',
    miners: 'pool/pool.miners',
    user: 'users/',
    every: 60000,
};

export default config;
