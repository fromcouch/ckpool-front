/**
 * Created by victor on 09/01/2018.
 */

import dom from '../helper/dom';
// import util from '../helper/utils';

export default class RenderController {
    constructor(config, ps) {
        this.config = config;
        this.ps = ps;
    }

    init() {
        const me = this;
        this.process(me);
        this.timeoutId = setInterval(me.process, me.config.every, me);
    }

    run() {
        const me = this;
        this.process(me);
    }

    process(me) {
        /* eslint class-methods-use-this: "off" */
        const data = dom.g(`${me.config.server}${me.config.status}`).catch(error => error);
        data.then((d) => {
            const poolData = d.split(/\r?\n/);
            poolData.pop();

            const t = poolData.map(e => JSON.parse(e));
            me.ps.publish('showData', t);

            return true;
        });
    }
}
